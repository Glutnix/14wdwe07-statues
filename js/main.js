(function () {

  var map;
  var statuesList;
  var markerCollection = [];
  var directionsService;
  var directionsDisplay;
  var userLocation;

  var ICONS = {
    'UNSELECTED': 'img/statue.png',
    'SELECTED':   'img/statue-select.png'
  };

  function initialise() {
    var mapOptions = {
      center: new google.maps.LatLng(-41.128314,174.944385),
      zoom: 9
    };
    map = new google.maps.Map(document.querySelector("#statues-map"), mapOptions);
    
    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(map);

    statuesList = document.querySelector("#statues-list");
    
    var resetMapButton = document.createElement("div");
    resetMapButton.innerHTML = "<button>Show All</button>";
    map.controls[google.maps.ControlPosition.TOP].push(resetMapButton);
    resetMapButton.addEventListener("click", function() {
      fitMapToBounds();
    });

    loadStatuesJSON();

    if (navigator.geolocation) {
      // geolocation is supported
      navigator.geolocation.getCurrentPosition(function (position) {
        userLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        var location_accuracy = new google.maps.Circle({
          'map': map,
          'center': userLocation,
          'radius': position.coords.accuracy,
          'fillColor': '#E95C42',
          'fillOpacity': 0.7,
          'strokeColor': '#FFFFFF', 
          'strokeOpacity': 0.7,
          'strokeWeight': 1
        });
        var location = new google.maps.Marker({
          'map': map,
          'position': userLocation
        });
      });
    }
  }

  google.maps.event.addDomListener(window, 'load', initialise);


  function loadStatuesJSON () {
  	jQuery.getJSON('js/statues.json', processStatuesJSON);
  }

  function processStatuesJSON(statues) {
  	console.log(statues);

    // sort statues array from north to south
    statues.sort(function (statueA, statueB) {
      if (statueA.lat < statueB.lat) {
        return 1; // A comes first - correct order
      } else {
        return -1; // B comes first - reverse the order
      }
    });

    statuesList.innerHTML = "";

    for(var i = 0; i < statues.length; i += 1) {
      var statue = statues[i];
      addMarker(statue);
    }

    fitMapToBounds();
  }

  function fitMapToBounds() {
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < markerCollection.length; i += 1) {
      bounds.extend(markerCollection[i].getPosition());
    }
    map.fitBounds(bounds);
  }

  function addMarker(location) {
    var marker = new google.maps.Marker({
      'map': map,
      'position': new google.maps.LatLng(location.lat, location.lng),
      'title': location.name,
      'icon': ICONS.UNSELECTED
    });

    markerCollection.push(marker);

    var listItem = document.createElement('li');
    listItem.innerHTML = "<a href='#'>" + location.name + "</a>";
    statuesList.appendChild(listItem);

    var infoWindow = new google.maps.InfoWindow({
      'content': "<div><h3>" + location.name + "</h3><p>" + location.content + "</p></div>"
    });

    listItem.addEventListener("click", function (evt) {
      evt.preventDefault();
      selectMarker(marker, listItem);
      setDirectionsTo(marker);
    });

    google.maps.event.addDomListener(marker, "click", function(){
      selectMarker(marker, listItem);
      infoWindow.open(map, marker);
      setDirectionsTo(marker);
    });
  }

  function selectMarker(marker, listItem) {
    deselectAllMarkers();
    marker.setIcon(ICONS.SELECTED);
    marker.setZIndex(1000);
    //map.setZoom(12);
    map.panTo(marker.getPosition());
    listItem.className = "active";
  }

  function deselectAllMarkers() {
    for (var i = 0; i < markerCollection.length; i += 1) {
      markerCollection[i].setIcon(ICONS.UNSELECTED);
      markerCollection[i].setZIndex(null);
    }
    listItemCollection = statuesList.querySelectorAll('li');
    for (var i = 0; i < listItemCollection.length; i += 1) {
      listItemCollection[i].className = "";
    }
  }

  function setDirectionsTo(marker) {
    if (userLocation) {
      var routeRequest = {
        origin: userLocation,
        destination: marker.getPosition(),
        travelMode: google.maps.TravelMode.WALKING
      };
      directionsService.route(routeRequest, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
        }
      })
    }
  }

})();